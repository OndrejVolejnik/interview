package api.core;

import lombok.Getter;

@Getter
public class StatusLine {
    private final int statusCode;
    private final String reasonPhrase;

    public StatusLine(org.apache.http.StatusLine raw) {
        statusCode = raw.getStatusCode();
        reasonPhrase = raw.getReasonPhrase();
    }
}

package api.calculator;

import api.core.Response;

import java.io.IOException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import static api.calculator.Operation.*;

public class StepDefinitions {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
    private final RestOperator restOperator;
    private Response lastResponse;

    public StepDefinitions(String url, String appPath) {
        restOperator = new RestOperator(url, appPath);
    }

    public void getAdd(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaGet(ADD, input1, input2);
    }

    public void getSubtract(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaGet(SUBTRACT, input1, input2);
    }

    public void getMultiply(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaGet(MULTIPLY, input1, input2);
    }

    public void getDivide(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaGet(DIVIDE, input1, input2);
    }

    public void getAdd(String input1, String input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaGet(ADD, input1, input2);
    }

    public void postAdd(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = lastResponse = restOperator.callViaPost(ADD, input1, input2);
    }

    public void postSubtract(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaPost(SUBTRACT, input1, input2);
    }

    public void postMultiply(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaPost(MULTIPLY, input1, input2);
    }

    public void postDivide(int input1, int input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaPost(DIVIDE, input1, input2);
    }

    public void postAdd(String input1, String input2) throws IOException, URISyntaxException {
        lastResponse = restOperator.callViaPost(ADD, input1, input2);
    }

    public void assertResponseCode(int expected) {
        int actual = lastResponse.getStatusLine().getStatusCode();
        LOGGER.info("Asserting Response code... Expected: " + expected + " Actual: " + actual);
        assert expected == actual;
    }

    public void assertNotResponseCode(int expected) {
        int actual = lastResponse.getStatusLine().getStatusCode();
        LOGGER.info("Asserting Response code... Not Expected: " + expected + " Actual: " + actual);
        assert expected != lastResponse.getStatusLine().getStatusCode();
    }

    public void assertResult(int expected) {
        int actual = lastResponse.getBody().getContent().getInt("result");
        LOGGER.info("Asserting Result... Expected: " + expected + " Actual: " + actual);
        assert expected == actual;
    }
}

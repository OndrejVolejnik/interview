# Test approach #

This document summarizes used approach.

### Initial considerations ###

* Tests of REST API, SOAP API and GUI should all be automated.
* REST API test are the least time-consuming, therefore they will go first.
* JUnit will be used for running tests in suites.
* Because the app is to be deployed manually, using Docker would be great, but too time-consuming.
* localhost:8080/ataccama/ will be used as URI for the deployed application
* java.util.logging will be used for logging purposes
* Tests will be run via maven compiler plugin

### Cases that must be covered by tests ###

* Division by zero
* Integer overflow
* Integer underflow
* Division that will not result in an integer
* Usual case of addition
* Usual case of subtraction
* Usual case of multiplication
* Usual case of division
* Addition with negative numbers
* Subtraction with negative numbers
* Multiplication with negative numbers
* Division with negative numbers
* Non-integer input
package api.core;

import com.google.common.io.CharStreams;
import lombok.Getter;
import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

@Getter
public class Body {
    private final String contentType;
    private final long contentLength;
    private JSONObject content;

    public Body(HttpEntity raw) throws IOException {
        contentType = raw.getContentType().getValue();
        contentLength = raw.getContentLength();
        Reader reader = new InputStreamReader(raw.getContent());
        String contentString = CharStreams.toString(reader);
        try{
            content = new JSONObject(contentString);
        } catch (JSONException e) {
            content = null;
            System.out.println("Could not parse: " + contentString);
        }
    }
}

package api.core;

public class PostEntityBuilder {
    private StringBuilder builder;
    private boolean hasFirstNode = false;

    public PostEntityBuilder() {
        init();
    }

    private void init() {
        builder = new StringBuilder();
        builder.append("{");
    }

    public void add(String key, String value) {
        builder.append("\"").append(key).append("\"").append(":").append("\"").append(value).append("\"");
        builder.append(",");
    }

    public void add(String key, int value) {
        builder.append("\"").append(key).append("\"").append(":").append(value);
        builder.append(",");
    }

    public void add(String key, float value) {
        builder.append("\"").append(key).append("\"").append(":").append(value);
        builder.append(",");
    }

    public void add(String key, double value) {
        builder.append("\"").append(key).append("\"").append(":").append(value);
        builder.append(",");
    }

    public void add(String key, boolean value) {
        builder.append("\"").append(key).append("\"").append(":").append(value);
        builder.append(",");
    }

    public String build() {
        builder.setCharAt(builder.length() - 1, '}');
        return builder.toString();
    }
}

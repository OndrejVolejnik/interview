package api;

import api.calculator.StepDefinitions;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class GetCalculatorTest {
    private final String url = "http://localhost:8080";
    private final String appPath = "ataccama/restWS/";
    private final StepDefinitions steps = new StepDefinitions(url, appPath);

    @Test
    public void standardAdd() throws IOException, URISyntaxException {
        steps.getAdd(1, 1);
        steps.assertResponseCode(200);
        steps.assertResult(2);
    }

    @Test
    public void standardSubtract() throws IOException, URISyntaxException {
        steps.getSubtract(1, 1);
        steps.assertResponseCode(200);
        steps.assertResult(0);
    }

    @Test
    public void standardMultiply() throws IOException, URISyntaxException {
        steps.getMultiply(2, 2);
        steps.assertResponseCode(200);
        steps.assertResult(4);
    }

    @Test
    public void standardDivide() throws IOException, URISyntaxException {
        steps.getDivide(2, 2);
        steps.assertResponseCode(200);
        steps.assertResult(1);
    }

    @Test
    public void divideByZero() throws IOException, URISyntaxException {
        steps.getDivide(1, 0);
        steps.assertNotResponseCode(200);
    }

    @Test
    public void integerOverflowAddition() throws IOException, URISyntaxException {
        steps.getAdd(Integer.MAX_VALUE, 1);
        steps.assertNotResponseCode(200);
    }

    @Test
    public void integerOverflowMultiplication() throws IOException, URISyntaxException {
        steps.getMultiply(Integer.MAX_VALUE, 2);
        steps.assertNotResponseCode(200);
    }

    @Test
    public void integerUnderflowSubtraction() throws IOException, URISyntaxException {
        steps.getSubtract(Integer.MIN_VALUE, 1);
        steps.assertNotResponseCode(200);
    }

    @Test
    public void integerUnderflowMultiplication() throws IOException, URISyntaxException {
        steps.getMultiply(Integer.MIN_VALUE, 2);
        steps.assertNotResponseCode(200);
    }

    @Test
    public void divisionRounding() throws IOException, URISyntaxException {
        steps.getDivide(5, 2);
        steps.assertResponseCode(200);
        steps.assertResult(3);
    }

    @Test
    public void multiplyNegative() throws IOException, URISyntaxException {
        steps.getMultiply(-2, 2);
        steps.assertResponseCode(200);
        steps.assertResult(-4);
    }

    @Test
    public void invalidInput() throws IOException, URISyntaxException {
        steps.postAdd("a", "b");
        steps.assertNotResponseCode(200);
    }
}

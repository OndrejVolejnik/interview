package api.calculator;

import api.core.HttpEngine;
import api.core.PostEntityBuilder;
import api.core.Response;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

public class RestOperator {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());
    private static final String INPUT_1 = "val1";
    private static final String INPUT_2 = "val2";

    private final String appPath;
    private final String url;
    private final HttpEngine http = new HttpEngine();

    public RestOperator(String url, String appPath) {
        this.url = url;
        this.appPath = appPath;
    }

    public Response callViaGet(Operation operation, int input1, int input2) throws URISyntaxException,
                                                                                   IOException {
        URIBuilder uriBuilder = new URIBuilder(url);
        uriBuilder.setPath(appPath + operation.getHttpGetValue());
        uriBuilder.addParameter(INPUT_1, String.valueOf(input1));
        uriBuilder.addParameter(INPUT_2, String.valueOf(input2));
        URI uri = uriBuilder.build();
        LOGGER.info("Calling GET " + uri.toString());
        return http.get(uri);
    }

    public Response callViaGet(Operation operation, String input1, String input2) throws URISyntaxException,
                                                                                   IOException {
        URIBuilder uriBuilder = new URIBuilder(url);
        uriBuilder.setPath(appPath + operation.getHttpGetValue());
        uriBuilder.addParameter(INPUT_1, String.valueOf(input1));
        uriBuilder.addParameter(INPUT_2, String.valueOf(input2));
        URI uri = uriBuilder.build();
        LOGGER.info("Calling GET " + uri.toString());
        return http.get(uri);
    }

    public Response callViaPost(Operation operation, int input1, int input2) throws URISyntaxException,
                                                                                    IOException {
        URIBuilder uriBuilder = new URIBuilder(url);
        uriBuilder.setPath(appPath + "compute");
        PostEntityBuilder jsonBuilder = new PostEntityBuilder();
        jsonBuilder.add(INPUT_1, input1);
        jsonBuilder.add(INPUT_2, input2);
        jsonBuilder.add("operation", operation.getHttpPostValue());
        URI uri = uriBuilder.build();
        String jsonBody = jsonBuilder.build();
        LOGGER.info("Calling POST " + uri.toString() + " with body: " + jsonBody);
        return http.post(uri, jsonBody);
    }

    public Response callViaPost(Operation operation, String input1, String input2) throws URISyntaxException,
                                                                                    IOException {
        URIBuilder uriBuilder = new URIBuilder(url);
        uriBuilder.setPath(appPath + "compute");
        PostEntityBuilder jsonBuilder = new PostEntityBuilder();
        jsonBuilder.add(INPUT_1, input1);
        jsonBuilder.add(INPUT_2, input2);
        jsonBuilder.add("operation", operation.getHttpPostValue());
        URI uri = uriBuilder.build();
        String jsonBody = jsonBuilder.build();
        LOGGER.info("Calling POST " + uri.toString() + " with body: " + jsonBody);
        return http.post(uri, jsonBody);
    }
}

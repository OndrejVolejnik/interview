package api;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({GetCalculatorTest.class, PostCalculatorTest.class})
public class FullScopeSuite {
}

package api.core;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.net.URI;

public class HttpEngine {
    private CloseableHttpClient client;

    private void init() {
        client = HttpClientBuilder.create().build();
    }

    public Response get(URI uri) throws IOException {
        init();
        HttpGet request = new HttpGet(uri);
        Response response = new Response(client.execute(request));
        client.close();
        return response;
    }

    public Response post(URI uri, String body) throws IOException {
        init();
        HttpPost request = new HttpPost(uri);
        request.setEntity(new StringEntity(body));
        request.setHeader("Content-Type", "application/json");
        Response response = new Response(client.execute(request));
        client.close();
        return response;
    }
}

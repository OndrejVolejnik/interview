package api.calculator;

import lombok.Getter;

public enum Operation {
    ADD("add", "add"),
    SUBTRACT("subtract", "sub"),
    MULTIPLY("multiply", "mul"),
    DIVIDE("divide", "div");

    @Getter
    private final String httpGetValue;
    @Getter
    private final String httpPostValue;

    Operation(String httpGetValue, String httpPostValue) {
        this.httpGetValue = httpGetValue;
        this.httpPostValue = httpPostValue;
    }
}

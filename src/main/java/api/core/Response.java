package api.core;

import lombok.Getter;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.IOException;

@Getter
public class Response {
    private final StatusLine statusLine;
    private final Header[] headers;
    private final Body body;

    public Response(CloseableHttpResponse raw) throws IOException {
        statusLine = new StatusLine(raw.getStatusLine());
        headers = raw.getAllHeaders();
        body = new Body(raw.getEntity());
    }
}
